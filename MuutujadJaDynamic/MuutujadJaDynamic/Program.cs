﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

namespace MuutujadJaDynamic
{
    class PropBag : DynamicObject
    {
        public Dictionary<string, dynamic> prop = new Dictionary<string, dynamic>();
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            prop[binder.Name] = value;
            return true;
        }
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = prop[binder.Name];
            return true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // meenutame, mis on muutuja ja muutuja tüüp
            int i = 7;
            string s = "Henn";

            dynamic d = i;
            Console.WriteLine(d++);
            d = s;

            dynamic rahakott = new PropBag();

            rahakott.Raha = 700;
            rahakott.Nimi = "Henn";

            Console.WriteLine(rahakott.prop["Raha"]);

            

        }
    }
}
