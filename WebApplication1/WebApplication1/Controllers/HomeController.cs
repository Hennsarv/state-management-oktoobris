﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {

        dynamic TempBag => new UniversalBag(TempData);
        dynamic AppBag => new UniversalBag(System.Web.HttpContext.Current.Application);
        dynamic SessBag => new UniversalBag(System.Web.HttpContext.Current.Session);

        public HomeController() : base()
        {
            ViewBag.Session = SessBag;
            ViewBag.Application = AppBag;
            ViewBag.Temp = TempBag;
        }

        public ActionResult Index(int? id, string nimi = "", string linn = "Tallinn")
        {
            //ViewBag.Tere = "Tere Henn! ";
            TempData["Tere"] = "Ohoo tereisekahh";
            ViewBag.Param = $"id={id} nimi={nimi} linn={linn}";
            ViewBag.QueryString = Request.QueryString;
            ViewBag.Vanaema = Request["Vanaema"];

            SessBag.Loendur++;
            AppBag.Loendur++;


            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}